import { GetAllPeople } from '../../getSwapi/__generated__/GetAllPeople';

export interface HomepageState {
    allPeople: GetAllPeople['allPeople'];
}