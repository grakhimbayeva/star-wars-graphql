import React, { useEffect } from 'react';
import getSwapi from '../../getSwapi';

interface IHomepageProps {

}

export const Homepage = (props: IHomepageProps) => {
    const fetchAllPeople = async () => {
        const fetchedPeople = await getSwapi.getAllPeople(5,10);
        console.log(fetchedPeople);
    }

    useEffect(()=> {
        fetchAllPeople();
    }, []);

    return (
        <div>
            <h1>Header</h1>
        </div>
    )
}