import { createSlice } from '@reduxjs/toolkit';
import { HomepageState } from './types';


const initialState: HomepageState = {
    allPeople: null,
}

const HomepageSlice = createSlice({
    name: 'homepage',
    initialState,
    reducers: {
        setHomepage(state, action) {
            state.allPeople = action.payload;
        }
    }
});

export const { setHomepage } = HomepageSlice.actions;
export default HomepageSlice.reducer;