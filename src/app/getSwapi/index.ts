import { apolloClient } from "../graphql";
import { GET_PEOPLE } from "./queries";

class getSwapi {
    async getAllPeople(first: Number, last: Number) {
        const response = await apolloClient.query({ query: GET_PEOPLE, variables: { first, last } });
        if (!response || !response.data) {
            throw new Error("Error fetching All People");
        }
        return response.data;
    }
}

export default new getSwapi();