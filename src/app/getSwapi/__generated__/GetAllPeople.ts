/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetAllPeople
// ====================================================

export interface GetAllPeople_allPeople_people_homeworld {
  __typename: "Planet";
  /**
   * The name of this planet.
   */
  name: string | null;
}

export interface GetAllPeople_allPeople_people_species {
  __typename: "Species";
  /**
   * The name of this species.
   */
  name: string | null;
}

export interface GetAllPeople_allPeople_people {
  __typename: "Person";
  /**
   * The ID of an object
   */
  id: string;
  /**
   * The name of this person.
   */
  name: string | null;
  /**
   * A planet that this person was born on or inhabits.
   */
  homeworld: GetAllPeople_allPeople_people_homeworld | null;
  /**
   * The birth year of the person, using the in-universe standard of BBY or ABY -
   * Before the Battle of Yavin or After the Battle of Yavin. The Battle of Yavin is
   * a battle that occurs at the end of Star Wars episode IV: A New Hope.
   */
  birthYear: string | null;
  /**
   * The gender of this person. Either "Male", "Female" or "unknown",
   * "n/a" if the person does not have a gender.
   */
  gender: string | null;
  /**
   * The species that this person belongs to, or null if unknown.
   */
  species: GetAllPeople_allPeople_people_species | null;
}

export interface GetAllPeople_allPeople {
  __typename: "PeopleConnection";
  /**
   * A list of all of the objects returned in the connection. This is a convenience
   * field provided for quickly exploring the API; rather than querying for
   * "{ edges { node } }" when no edge data is needed, this field can be be used
   * instead. Note that when clients like Relay need to fetch the "cursor" field on
   * the edge to enable efficient pagination, this shortcut cannot be used, and the
   * full "{ edges { node } }" version should be used instead.
   */
  people: (GetAllPeople_allPeople_people | null)[] | null;
}

export interface GetAllPeople {
  allPeople: GetAllPeople_allPeople | null;
}

export interface GetAllPeopleVariables {
  after?: string | null;
  first?: number | null;
  before?: string | null;
  last?: number | null;
}
