import gql from 'graphql-tag';

export const GET_PEOPLE = gql`
    query GetAllPeople($after: String, $first: Int, $before: String, $last: Int) {
        allPeople(after: $after, first: $first, before: $before, last: $last) {
            people {
                id,
                name,
                homeworld {
                    name
                },
                birthYear,
                gender,
                species {
                    name
                },
            }
        }
    }
`;