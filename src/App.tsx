import React from 'react';
import './App.scss';
import { Homepage } from './app/containers/Homepage';

function App() {

  return (
    <div className="app">
      <header className="app-header">Hello World</header>
        <Homepage />
    </div>
  );
}

export default App;
